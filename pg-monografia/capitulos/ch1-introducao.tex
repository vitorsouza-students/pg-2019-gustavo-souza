% ==============================================================================
% TCC - Gustavo Storck Andrade de Souza
% Capítulo 1 - Introdução
% ==============================================================================
\chapter{Introdução}
\label{sec-intro}
Vivemos em mundo cada vez mais conectado: estima-se que 56,1\% da população mundial tem acesso à Internet. Esse processo desencadeou mudanças profundas na sociedade. Em 1833 o serviço postal de Paris a Strasburgo levava 36 horas, a notícia da queda da bastilha chegou a Madri em 13 dias~\cite[p.~25]{historia}. Hoje podemos enviar uma mensagem para qualquer lugar do mundo em uma fração de segundos. Nossa forma de se comunicar, fazer compras, buscar informação, administrar nosso dinheiro foi totalmente alterada em menos de um século. 

No meio desse processo de evolução e transformação, um dos mecanismos fundamentais é o software, pois ele define como um determinado problema será efetivamente resolvido, fazendo a interface entre a demanda do usuário e o poder computacional para obter uma solução. Um dos principais desafios na elaboração de um software é desenvolver com qualidade, não bastando focarmos apenas no resultado do problema~\cite{code}. Certamente é importante alcançarmos um objetivo, seja ele fazer um cálculo ou realizarmos um pagamento a um amigo, mas também devemos dar valor ao processo de resolução do problema. Um software bem desenvolvido garante estabilidade, escalabilidade, fácil manutenção. Quando esses fatores são atingidos conseguimos uma melhor experiência do usuário, economia de dinheiro e tempo~\cite{architecture}. Este trabalho propõe estudar como podemos construir esse tipo software e então aplicar métodos adequados no desenvolvimento de um aplicativo, que visem  escalabilidade, testabilidade e manutenibilidade para conseguirmos atingir a qualidade.

%%% Início de seção. %%%
\section{Contexto}
\label{sec-intro-contexto}
O objetivo deste projeto será desenvolver um aplicativo iOS para a Eventu, um sistema para organização e gerência de eventos capaz de criar e personalizar um site e aplicativos nativos (Android e iOS) para apoio à realização de um evento (mais detalhes sobre os requisitos da Eventu são apresentados no Capítulo~\ref{sec-requisitos}).

Para o estudo e aplicação de métodos que garantam a qualidade, nos baseamos no processo de software descrito na Figura~\ref{fig-projeto_software}. Todas as partes foram contempladas, mas nosso maior foco foi na região destacada na figura. Aplicamos métodos ágeis e modernos que visem desburocratizar o processo e atingir o objetivo.

\begin{figure}[H]
\centering
\includegraphics[width=.80\textwidth]{figuras/introducao/projeto_software.png} 
\caption{Processo de software seguido neste trabalho.}
\label{fig-projeto_software}
\end{figure}



%%% Início de seção. %%%
\section{Motivação e Justificativa}
\label{sec-intro-motjus}

Em novembro de 2017, eu e meu sócio fundamos a Eventu, a demanda de mercado era clara, a maioria dos softwares para gerência de evento eram muito ruins e incompletos ou muito caros, suprindo mais que o necessário às demandas da organização. A ideia surgiu quando eu participava do PET (Programa de Ensino Tutorial) e trilhava meus primeiros passos no desenvolvimento de aplicativos móveis para Android. Para validar os conhecimentos aprendidos, estava construindo, em conjunto com a equipe, um aplicativo para auxiliar participantes do Enapet (Encontro Nacional dos Grupos do Programa de Educação Tutorial) e foi uma experiência muito enriquecedora.

Após o evento a tutora do grupo enxergou um grande potencial e me incentivou a empreender com essa ideia. Alguns meses depois, em 2018, após muito trabalho meu e de meu sócio, ele responsável pelo site e \textit{backend} e eu pelo aplicativo Android, concluímos a primeira versão do sistema. Nesse mesmo ano conseguimos dois clientes e seus eventos foram um sucesso. Porém, problemas aconteceram: o código era extremamente difícil de dar manutenção e adicionar novas funcionalidades; alguns \textit{bugs} eram relatados mas não tínhamos como simular ou saber em qual situação aconteceram; ao alterar parte do código acabávamos inserindo problemas em outras partes que o reutilizavam; o processo de versionamento e publicação era sem controle; dentre outros. Estava claro que algo tinha que mudar. 

No ano de 2018 também tive a oportunidade de trabalhar na PicPay,\footnote{\url{https://picpay.com/}} um aplicativo de pagamentos usados por milhões de pessoas. Essa experiência alterou completamente a minha forma de pensar em desenvolvimento de software. Foi o início da minha busca por processos e métodos de desenvolvimento capazes de assegurar qualidade do produto \cite{qualidade-software}.

Qualidade é um dos fatores primordiais para se garantir a evolução de um produto. É comum ouvirmos histórias de empresas que aumentaram o número de desenvolvedores e a média de código entregue diminui. Isso ocorre devido ao estado do código, da arquitetura e, de uma forma geral, aos processos de desenvolvimento. De acordo com \citeonline{qualidade-falbo}, a qualidade do produto depende fortemente da qualidade de seu processo de desenvolvimento. A falta de qualidade acontece pois muitas vezes colocamos velocidade de entrega em detrimento da qualidade e ficamos satisfeitos em entregar um projeto no prazo, sendo o maior objetivo apenas funcionar. Há um pensamento que traduz muito bem essa ideia:

\textit{If you give me a program that works perfectly but is impossible to change, then it won't work when the requirements change, and I won't be able to make it work. Therefore the program becomes useless.}

\textit{If you give me a program that does not work but is easy to change, then I can make it work, and keep working as requirements change. Therefore the program will remain continually useful.~\cite[p.~15]{architecture}}

Parte da culpa é nossa, como programadores, por não projetarmos e codificarmos da maneira correta. Essa foi a  maior motivação do projeto: produzir um software cujo resultado final agregue valor ao cliente e aos desenvolvedores, fazendo um \textit{trade-off} entre qualidade e tempo.

%%% Início de seção. %%%
\section{Objetivos}
\label{sec-intro-obj}
O objetivo geral deste trabalho é desenvolver um aplicativo iOS com o foco em desenvolvimento com qualidade, a ser atingido por meio da implementação de testes, módulos, padrões de código, aplicando uma esteira de desenvolvimento que implemente integração e entrega contínuas e tomando decisões arquiteturais que tornem o código mais escalável, reusável, isolado e de fácil manutenção.

São objetivos específicos do trabalho:
\begin{itemize}

\item Documentar os requisitos conforme os objetivos solicitados pela Eventu;

\item Prototipar uma interface que atenda a demanda do projeto e as \textit{guidelines} da Apple para aplicativos iOS;

\item Documentar o projeto de software, definindo sua arquitetura;

\item Organizar um \textit{pipeline} de desenvolvimento, que possa ser automatizado com o desenvolvimento;

\item Criar uma biblioteca privada que reúna os componentes gráficos que foram criados especificamente para Eventu;

\item Criar uma biblioteca pública de código aberto, para um componente gráfico que se adeque a outros projetos, usando como canal de distribuição o \textit{CocoaPods};\footnote{\url{https://cocoapods.org}}

\item Implementar testes unitários e de interface;

\item Organizar uma implantação que seja gradual e possua versionamento da base;

\item Usar essa solução no Congresso Brasileiro de Engenharia Agrícola(CONBEA), que ocorrerá na segundo semestre de 2019, de modo a coletar informações de uso e problemas no aplicativo, avaliando o resultado do projeto.
\end{itemize}


%%% Início de seção. %%%
\section{Método de Desenvolvimento do Trabalho}
\label{sec-intro-met}

No início do trabalho foram realizadas reuniões de requisitos e prototipação, para modelagem do sistema. As análises de requisitos tiveram como base os fundamentos teóricos da disciplina de Engenharia de Software. Após termos os requisitos iniciou-se o processo de prototipação usando como referencia o conteúdo da disciplina optativa Interface Humano Computador, além do estudo da \textit{Interface Guidelines}\footnote{\url{https://developer.apple.com/design/human-interface-guidelines/ios/overview/themes/}} da Apple, usando o Figma\footnote{\url{https://www.figma.com/}} como software de desenho das telas. Os artefatos gerados por esses dois processos foram utilizados como descrição das tarefas, cadastradas no GitProject,\footnote{\url{https://github.com/features/project-management/}} para acompanhamento do projeto, esses artefatos são apresentados no Capítulo \ref{sec-requisitos}.

Para definir e organizar um pipeline de desenvolvimento foi utilizado como base o conhecimento adquirido em participação de eventos focados no assunto, estudos de casos de grandes empresas de tecnologia, \textit{podcasts} e leitura de artigos. As ferramentas auxiliares nesse passo foram definidas através de pesquisas e testes das existentes no mercado, levantando custo vs. benefício para sua implantação, com apoio de suas documentações. As ferramentas e suas implementações estão disponíveis no Capítulo \ref{sec-apoio}.

Para temas amplos que exigem conhecimento vasto, como definição da arquitetura, linguagem de programação Swift,\footnote{\url{https://swift.org/documentation/}} desenvolvimento iOS e testes, me baseei em livros e artigos que discutem as escolhas levando em consideração a qualidade como argumento. No desenvolvimento do projeto foram utilizadas bibliotecas de terceiros que auxiliaram a atingir alguns objetivos como organizar uma implantação que seja gradual e possua versionamento da base implementado a partir do \textit{Remote Config}\footnote{\url{https://firebase.google.com/docs/remote-config}} do \textit{Firebase}.\footnote{\url{https://firebase.google.com}} Algumas dessas bibliotecas que eram de código aberto serviram de referencial para implementarmos as nossas próprias bibliotecas públicas e privadas e fazer a distribuição no gerenciador de dependência CocoaPods.\footnote{\url{https://cocoapods.org}} Também nos baseamos em padrões de projetos e código para construção do projeto, todos esses métodos em conjunto com a documentação da arquitetura podem ser vistos no Capítulo \ref{sec-projeto}.

Como resultado aplicamos todos os conceitos apresentados no trabalho para desenvolvimento de um aplicativo iOS, usado no Congresso Brasileiro de Engenharia Agrícola (CONBEA) 2019.

Pretende-se mostrar, com este trabalho, que a graduação forneceu a base sólida, para que eu continuasse a aprender, evoluir e analisar com criticidade novas ideias propostas. 

%%% Início de seção. %%%
\section{Organização do Texto}
\label{sec-intro-org}
Esta monografia, entregue como produto final deste trabalho, é dividida da seguinte maneira:

\begin{itemize}
\item Capítulo \ref{sec-referencia} -- Fundamentação Teórica e Tecnologias Utilizadas: apresenta uma discussão sobre tecnologias para desenvolvimento \textit{mobile} e introduz as principais ferramentas que foram utilizadas no decorrer do projeto;
\item Capítulo \ref{sec-apoio} -- Apoio ao Desenvolvimento: apresenta todas as ferramentas e métodos que foram utilizados para garantir ou auxiliar um desenvolvimento com qualidade e escalabilidade;
\item Capítulo \ref{sec-requisitos} -- Especificação de Requisitos: apresenta o diagrama de classes UML, a tabela de requisitos e o protótipo da interface;
\item Capítulo \ref{sec-projeto} -- Projeto e Implementação: descreve a implementação das principais tecnologias e conceitos utilizados neste trabalho e apresenta todo o sistema desenvolvido, descrevendo os fluxos e comportamentos, ilustrados por capturas de tela;
\item Capítulo \ref{sec-conclusoes} -- Considerações Finais: apresenta as conclusões do trabalho, dificuldades e ensinamentos que o projeto proporcionou;
\end{itemize}
