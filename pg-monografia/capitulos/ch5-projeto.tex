% ==============================================================================
% TCC - Gustavo Storck Andrade de Souza
% Capítulo 5 - Implementação
% ==============================================================================
\chapter{Projeto e Implementação}
\label{sec-projeto}

Neste capítulo discutimos sobre as escolhas de implementação feitas durante o trabalho. A Seção~\ref{sec-module}, exibe os módulos e arquitetura utilizadas no projeto. A Seção~\ref{sec-views} mostra o processo que utilizamos na implementação das \textit{views}. Na Seção~\ref{sec-testes} apresentamos os tipos de testes feitos e quais padrões foram adotados. A Seção~\ref{sec-firebase} apresenta as bibliotecas do Firebase que foram adotadas com a respectiva discussão sobre sua utilização. E por fim na Seção~\ref{sec-sistema} é apresentado todo o sistema por meio de uma série de capturas de telas.


%%% Início de seção %%%
\section{Módulos}
\label{sec-module}

Módulos são definidos por uma unidade separada de projeto, eles podem ser
desenvolvidos e distribuídos de forma independente e serem reutilizados por diversas
aplicações. A Figura~\ref{fig-imple-depen_externa} exibe todas dependências externas que temos no \textit{App} da Eventu. Os módulos \textbf{UI} e \textbf{Networking} foram criados para atender ao domínio específico da Eventu, por isso são de caráter privado se integram ao projeto como qualquer outro módulo de terceiros. O módulo de \textbf{UI} reúne os componentes gráficos que foram criados especificamente para Eventu, fornecendo implementações comuns de \textit{layout}, contendo todos os componentes usados no aplicativo, além de fornecer recursos como cor, tipografia e espaçamento, e encapsular algumas bibliotecas externas como a de carregamento de imagens e de animação. O segundo é o módulo de \textbf{Networking}, responsável por fornecer a implementação de acessos a recursos da Internet usando o protocolo \textit{HTTP} e o estilo arquitetural \textit{REST~\cite{rest}} utlizando o Alomofire.\footnote{\url{https://github.com/Alamofire/Alamofire}}

\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{figuras/implementacao/implementacao_dependencia_externa.png}
\caption{Dependências externas}
\label{fig-imple-depen_externa}
\end{figure}

\textbf{EventuTeste} e \textbf{EventuUITests}, são os módulos responsáveis por executar os testes unitários e de interface, respectivamente. Os módulos relativos ao \textbf{Firebase} foram de grande importância para atendermos as especificações do projeto e serão explicados em mais detalhes na Seção~\ref{sec-firebase}. Por fim, os módulos localizados no topo da imagem, são implementações visuais de algum componente que despenderiam muito tempo para serem feitos por conta própria. Todos os módulos externos antes de serem adotadas passaram por uma avaliação de documentação, suporte e apoio da comunidade. Tentamos isolá-los da melhor forma possível para que, caso exista a necessidade de substituição, isso seja feito com a maior segurança e no menor tempo possível.

Adicionalmente também foi desenvolvido um terceiro módulo, chamado \textit{ProgressStepView},\footnote{\url{https://github.com/gustavoSAS/ProgressStepView}} que fornece a implementação de um componente gráfico, não disponível nativamente no iOS, de controle de progresso animado. Ele não é usado no aplicativo, a sua concepção e publicação foi para demonstrar os conceitos aprendidos durante esse trabalho na criação, utilização e distribuição de módulos usando o gerenciador de dependência \textit{CocoaPods}.\footnote{\url{https://cocoapods.org/}} Esse módulo está disponível da mesma forma que outras 73 mil bibliotecas criadas pela comunidade de desenvolvimento iOS e por grander empresas como Google\footnote{\url{https://github.com/airbnb/lottie-ios}} e Airbnb.\footnote{\url{https://github.com/airbnb}}

A Figura~\ref{fig-imple-interna} exibe a divisão interna das funcionalidades, cada funcionalidade é composta por uma ou mais telas, cada tela é desenvolvida baseada em uma arquitetura, que chamamos de \emph{cena}, que provê a implementação e divisão das responsabilidades.
 
\begin{figure}[H]
\centering
\includegraphics[width=.7\textwidth]{figuras/implementacao/implementacao_interna.png}
\caption{Estrutura interna Eventu}
\label{fig-imple-interna}
\end{figure}

Para implementação do nosso projeto usamos uma arquitetura mista, aproveitando as melhores características das arquiteturas VIP e VIPER, discutidas na Seção~\ref{sec-architeture}. Utilizamos como base a VIP, por ser uma arquitetura mais aberta definindo apenas os atores principais e possuir um fluxo unidirecional de dados. A Figura~\ref{fig-arquitetura-projeto} apresenta a arquitetura do projeto.

\begin{figure}[H]
\centering
\includegraphics[width=.80\textwidth]{figuras/referencial/arquitetura-projeto.png} 
\caption{Arquitetura do Projeto.}
\label{fig-arquitetura-projeto}
\end{figure}

Adicionamos um \textit{Worker} para tratar a recuperação de dados da Web ou do banco de dados local, fazendo gerenciamento das \textit{threads} e requisições. O \textit{Coordinator} faz o controle do fluxo, e só pode ser acessado depois de todo um ciclo, onde serão analisadas regras de negócio e apresentação. O \textit{Factory} tem a responsabilidade de instanciar e injetar as dependências necessárias para criar uma nova cena. O \textit{Presenter} cuida da lógica de apresentação e avisa ao \textit{Controller} quando e como ele deve exibir os dados. Essa proposta visa distribuir as responsabilidades e diminuir o acoplamento entre as camadas. Além de se basear no VIPER, a proposta se inspira também no padrão hexagonal.\footnote{\url{https://java-design-patterns.com/patterns/hexagonal/}}

%%% Início de seção %%%
\section{Views}
\label{sec-views}

A implementação de todas as cenas do \textit{App} é feita utilizando \textit{View Code}, por ter se mostrado uma forma muito mais segura e flexível de desenvolvimento. Quando utilizamos blocos, variáveis e funções de certa forma se torna muito mais fácil criar uma \textit{view} reutilizável e de fácil manutenção. Também podemos perceber o incentivo da Apple na programação de \textit{views} via código com o lançamento do SwiftUI.\footnote{\url{https://developer.apple.com/xcode/swiftui/}}

Definimos alguns padrões na declaração e composição dos elementos. Na declaração os elementos sempre começam com o modificador de acesso \textbf{private}, as atribuições de propriedades só podem ser feitas através de métodos públicos ou variáveis computadas, dessa forma nunca expomos diretamente a implementação da \textit{view}. Em seguida são declarados como \textbf{lazy}, pois a inicialização tardia traz algumas vantagens, como a instância só ocorre na primeira vez em que a variável é usada e alocação de memória é feita apenas se necessário e no momento em que for demandada. Também podemos usar o \textbf{self}, pelo fato da inicialização acontecer após a execução do construtor da classe.
Quanto à composição, ela acontece em 3 momentos. No primeiro usamos a inicialização via \textit{closure} para compor as propriedades do elemento, no segundo montamos toda hierarquia da \textit{view} na chamada da função \textbf{addComponents}. E por último no método \textbf{layoutComponents} definimos suas \textit{constraints} e prioridades. A Listagem~\ref{codigo_view_code} exibe o exemplo de uma \textit{view} conforme discutido.

\newpage
\lstinputlisting[label={codigo_view_code}, caption={Exemplo de View Code}, float={htpb}]{codigos/codigo_view_code.swift}

%%% Início de seção %%%
\section{Testes}
\label{sec-testes}

Em nossa experiência, testes são uma das melhores ferramentas disponíveis para a tarefa de programação: ao mesmo tempo que ele consegue eliminar a angústia de saber se o que estamos alterando ou adicionando irá afetar de forma negativa algum pedaço da nossa aplicação, também nos poupa o árduo trabalho de ficar re-testando a aplicação em diversos contextos atrás de alguma falha de implementação, muitas vezes acabam promovendo o desacoplamento de código, melhoram nossa escrita de código e servem de documentação.

Assim como outras partes do aplicativo, os testes também irão sofrer refatoração, manutenção e adição. Por isso é muito importante implementar padrões de escrita e estruturas~\cite{code}.

Para nomenclatura das funções foi adotado o padrão \textit{Test When Should}: esses três blocos são separados por hífen para facilitar a leitura. O bloco de \textit{Test} diz qual função estamos testando, \textit{When} define em qual contexto ou situação o teste ocorrerá, \textit{Should} descreve o comportamento esperado para o teste. A Listagem~\ref{codigo_nomeclatura_teste} mostra um exemplo de teste que ilustra esta nomenclatura.

\lstinputlisting[label={codigo_nomeclatura_teste}, caption={Exemplo de nomeclatura para teste}, float={htpb}]{codigos/codigo_nomeclatura_teste.swift}

Para compor a estrutura de teste foram usados os conceitos de \textit{Sut, Mock, Spy}:\footnote{\url{https://martinfowler.com/articles/mocksArentStubs.html}}

\begin{itemize}

\item \textbf{\textit{Sut (Subject Under Testing)}}: representa o objeto que queremos testar;

\item \textbf{\textit{Mock}}: representa a falsificação de uma dependência, retornando um caso de uso conforme necessário, mas obedecendo o contrato pré-definido. Como exemplo não podemos utilizar a camada real de \textit{Service} nos testes, pois deste modo limitamos nossos testes a disponibilidade do servidor, nesse caso criamos um \textit{Mock} desse \textit{Service}, que irá nos trazer retornos pré estabelecidos conforme nossa necessidade;

\item \textbf{\textit{Spy}}: é um objeto que grava suas interações com outros objetos, ele é útil quando precisamos saber se a função que estamos testando chamou alguma outra classe ou quais tipos de argumentos foram passados.

\end{itemize}

Um exemplo de escritas de teste envolvendo todos esses conceitos é exibida na Listagem~\ref{codigo_atores_teste}.

\newpage
\lstinputlisting[label={codigo_atores_teste}, caption={Exemplo de atores  para teste}, float={htpb}]{codigos/codigo_atores_teste.swift}

Para o nosso projeto implementamos testes unitários e de interface. Os testes unitários foram implementados nas camadas \textit{Interactor} e \textit{Presenter} pois, devido à arquitetura proposta, essas classes concentram toda lógica responsável por uma cena.

Os testes de interface, por serem muitos custosos, foram implementadas em fluxos essenciais da aplicação, sendo nossa principal preocupação nesse ponto testar se o usuário consegue fazer uma determinada navegação. Aqui buscamos fugir de testes que checam tamanho, fonte e cor de componentes, para esse caso seria mais conveniente fazer um teste de \textit{SnapShot}. Foi escolhido o teste de navegação pois ao focarmos somente em fluxos importantes, economizamos tempo de desenvolvimento e \textit{CI}, porque não testamos o comportamento de todas as funcionalidades do Aplicativo. 

Todos os testes foram implementados usando a biblioteca nativa \textit{XCTest}.

%%% Início de seção %%%
\section{Firebase}
\label{sec-firebase}

Firebase é uma plataforma do Google que oferece vários serviços que auxiliam na criação e escalabilidade de \textit{Apps mobile}, essa seção se destina a mostrar quais serviços do Firebase foram utilizados.

%%% Início de seção. %%%
\subsection{\textit{Cloud Storage}}
\label{sec-firebase-storage}

É um serviço de armazenamento de alta performance e escalabilidade, usado para armazenar conteúdos gerados pelo usuário, como áudio, vídeo e documentos. Para o projeto Eventu, ele é utilizado para o armazenamento de imagens do usuário. O usuário pode definir uma imagem em 3 pontos do \textit{App}, conforme ilustrado na Figura~\ref{fig-firebase-storage-imagem}:

\begin{itemize}

\item Ao se cadastrar via e-mail;

\item Editar a foto nas configurações;

\item Login via Google, neste caso é usada a imagem definida pelo usuário nos serviços do Google.

\end{itemize}

\begin{figure}[H]
\centering
\includegraphics[width=0.32\textwidth]{figuras/implementacao/imagem-cadastro.png}
\includegraphics[width=0.32\textwidth]{figuras/implementacao/imagem-ajustes.png}
\includegraphics[width=0.32\textwidth]{figuras/implementacao/imagem-google.png}
\caption{Upload de imagem}
\label{fig-firebase-storage-imagem}
\end{figure}

Um dos maiores benefícios do \textit{Cloud Storage}\footnote{\url{https://firebase.google.com/docs/storage}} é cuidar inteiramente do \textit{upload} da imagem, tratando erros como, por exemplo, se a conexão com a Internet cair no meio do processo de \textit{upload}, a biblioteca consegue iniciar o \textit{download} do último passo válido. Além disso é criada uma representação das imagens no console do Firebase, ilustrado na Figura~\ref{fig-firebase-storage}, onde é possível fazer algumas edições, acompanhar o tráfego, e definir regras de segurança.

\begin{figure}[H]
\centering
\includegraphics[width=0.9\textwidth]{figuras/implementacao/imagem-storage.png}
\caption{Painel Cloud Storage}
\label{fig-firebase-storage}
\end{figure}

Fluxo de \textit{upload}: ao solicitar o processo de \textit{upload}, o aplicativo comprime a imagem, cria uma URL única baseada no e-mail do usuário e chama a biblioteca passando a URL e a imagem comprimida. Ao concluir o processo de \textit{upload}, uma URL final é retornada referenciando o local onde a imagem está armazenada e essa URL é salva no modelo do usuário para ser usada em outros pontos do \textit{App}.

%%% Início de seção. %%%
\subsection{\textit{Analytics}}
\label{sec-firebase-analytics}

É uma solução de análise de \textit{Apps} que fornece informações sobre o uso do aplicativo e o envolvimento do usuário. O \textit{SDK} de \textit{Analytics}\footnote{\url{https://firebase.google.com/docs/analytics}} captura automaticamente uma série de eventos e propriedades do usuário, como tipo de celular, sexo, região, idade, quantidade de usuários online, retenção do usuário, além de permitir que você defina eventos personalizados. 

Isso permite traçarmos um perfil muito mais detalhado sobre o público do cliente, além de nos ajudar a entender como está a aceitação do \textit{App} para os usuários. O \textit{App} implementa eventos para cada tela que foi aberta e se o usuário conseguiu fazer determinada ação, como enviar uma pergunta ou fazer uma avaliação. A Figura~\ref{fig-firebase-evento-click} mostra a lista de eventos personalizados para o aplicativo desenvolvido.

\begin{figure}[H]
\centering
\includegraphics[width=0.9\textwidth]{figuras/implementacao/evento-click.png}
\caption{Eventos personalizados}
\label{fig-firebase-evento-click}
\end{figure}

Esse tipo de métrica nos ajuda a melhorar, criar, excluir, reorganizar funcionalidades, de forma que suas funções sejam mais intuitivas e relevantes. Podemos testar funcionalidades em determinado segmento, de região, idade, etc. É muito importante tomarmos decisões baseadas em dados, pois dessa forma nos tornamos mais assertivos.

Os dados capturados ficam disponíveis em um painel no Console do Firebase, como mostra a Figura~\ref{fig-firebase-evento-dados}.

\begin{figure}[H]
\centering
\includegraphics[width=0.70\textwidth]{figuras/implementacao/evento-dashboard.png}
\includegraphics[width=0.4\textwidth]{figuras/implementacao/evento-idade.png}
\caption{Painel de dados}
\label{fig-firebase-evento-dados}
\end{figure}

%%% Início de seção. %%%
\subsection{\textit{Crashlytics}}
\label{sec-firebase-crashlytics}

O \textit{Crashlytics}\footnote{\url{https://firebase.google.com/docs/crashlytics}} é uma ferramenta de relatório de falhas em tempo real que ajuda a monitorar, priorizar e corrigir problemas de estabilidade que comprometem a qualidade do seu aplicativo. Após sua integração com o App, é possível obter em detalhes informações sobre alguma falha, como versão do dispositivo, versão do sistema operacional, versão do aplicativo e um \textit{stack trace} detalhado. Com isso em mãos é muito mais fácil priorizar e reproduzir um problema. Foi uma excelente solução ao problema que ocorria há alguns anos, quando não conseguimos saber se usuários estavam sendo afetados por algum \textit{bug}, pois muitas vezes quando o problema era relatado de maneira manual era muito difícil saber sua origem. A Figura~\ref{fig-firebase-evento-crashlytics} mostra o painel do \textit{Crashlytics}.

\begin{figure}[H]
\centering
\includegraphics[width=0.95\textwidth]{figuras/implementacao/evento-crashlytics.png}
\caption{Painel Crashlytics}
\label{fig-firebase-evento-crashlytics}
\end{figure}

%%% Início de seção. %%%
\subsection{\textit{Authentication}}
\label{sec-firebase-auth}

Autenticação é um ponto muito delicado da maioria das aplicações, por questões de segurança tende a ser muito difícil e custosa de ser implementada do zero. O Firebase \textit{Authentication}\footnote{\url{https://firebase.google.com/docs/auth}} resolve nosso problema, fornecendo serviços de \textit{back-end, SDKs} prontas para autenticar usuários no \textit{App}. Com suporte à autenticação por meio de senhas, números de telefone e provedores como Google e Facebook. Login por provedores acabam sendo mais escolhidos pelos usuários devido à facilidade e velocidade.

Na implementação do Eventu, escolhemos a autenticação via e-mail/senha e pelo Google. O \textit{App} usa a biblioteca \textit{Authentication} para enviar um \textit{token} único por usuários em rotas que devem ser autenticadas, como atualizar perfil, enviar pergunta, fazer avaliação, etc. Nessas requisições é adicionado ao \textit{header} um campo \textit{Authorization}, que contém o \textit{token} válido por seção. Por questões de segurança, esse \textit{token} é renovado a cada 5 minutos. 

A biblioteca também possui suporte a cadastro. Por requisito de sistema, podemos ter dois tipos de cadastro: o primeiro é quando pode ser feito pelo \textit{App} utilizando e-mail/senha ou o provedor do Google, já o segundo ocorre quando o cadastro deve ser controlado por uma página Web e, nesse caso, apenas o login é feito pelo aplicativo, enquanto o botão de cadastro direciona para a página Web correspondente. A Figura~\ref{fig-firebase-auth-login} ilustra os diferentes métodos de login no aplicativo.

\begin{figure}[H]
\centering
\includegraphics[width=0.4\textwidth]{figuras/implementacao/login-email.png}
\includegraphics[width=0.4\textwidth]{figuras/implementacao/login-google.png}
\caption{Métodos de login}
\label{fig-firebase-auth-login}
\end{figure}

A biblioteca de \textit{Authentication}, também implementa métodos redefinir/recuperar senha, através do e-mail. Além de possuir um painel para configurações da funcionalidade, como mostra a Figura~\ref{fig-firebase-auth-painel}.

\begin{figure}[H]
\centering
\includegraphics[width=0.95\textwidth]{figuras/implementacao/auth-painel.png}
\caption{Painel com login dos usuários}
\label{fig-firebase-auth-painel}
\end{figure}

%%% Início de seção. %%%
\subsection{\textit{Remote Config}}
\label{sec-firebase-remote}

\textit{Remote Config}\footnote{\url{https://firebase.google.com/docs/remote-config}} é um serviço em nuvem que permite alterar um comportamento ou aparência do \textit{App} sem exigir que os usuários façam o \textit{download} de uma atualização. Isso abre diversas possibilidades, nos permite fazer teste A/B\footnote{\url{https://firebase.google.com/docs/ab-testing}} a um determinado segmento de usuários, podemos desabilitar um fluxo que não esteja funcionando corretamente, etc. 

No entanto, a principal vantagem para nós foi a versatilidade: na concepção do aplicativo definimos que ele tinha que ser customizável para atender a público e demandas diferentes, sem que exigisse muito esforço. O \textit{Remote Config} se encaixa perfeitamente como uma das formas de alcançar isso: o \textit{App} foi programado para disponibilizar e se comportar de acordo com algumas \textit{flags}, que podem ser habilitadas por evento de forma remota, como mostra a Figura~\ref{fig-firebase-flag-painel}. Então, por exemplo, se o cliente não quiser o fluxo de login no \textit{App}, basta desabilitar em um arquivo de configuração e pronto, não temos que mexer em uma linha de código. Além de velocidade, nos trás segurança, pois como esses comportamentos já são esperados podemos escrever testes, evitando que alguém mexa com pressa no código e acabe introduzindo um \textit{bug}.

\begin{figure}[H]
\centering
\includegraphics[width=0.95\textwidth]{figuras/implementacao/remote-config.png}
\caption{Painel com as Flags}
\label{fig-firebase-flag-painel}
\end{figure}
Através do \textit{Remote Config} também é possível organizar uma implantação que seja gradual e possua versionamento da base, podemos habilitar um fluxo ou funcionalidade, levando em consideração versão do aplicativo, sistema operacional, e porcentagem de usuários que desejamos atingir.

No entanto, como nem tudo é perfeito, o Google avisa que uma mudança pode demorar até 12 horas para refletir nos usuários, portanto temos que ficar muito atentos ao mexer nesse arquivo, pois 12 horas podem chegar a ser até um quarto da duração de um evento. 

%%https://developer.apple.com/library/archive/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/APNSOverview.html#//apple_ref/doc/uid/TP40008194-CH8-SW1
%%% Início de seção. %%%
\subsection{\textit{Cloud Messaging}}
\label{sec-firebase-messaging}

\textit{Cloud Messaging}\footnote{\url{https://firebase.google.com/docs/cloud-messaging}} é uma solução para envio confiável de notificações entre plataformas, podendo ser usada de 2 maneiras. A primeira como \textit{Push Notification} Silencioso, por meio da qual o servidor avisa para o aplicativo que existe algum dado disponível para atualização e esta pode fazer a busca e a atualização destes dados em \textit{background}. A segunda forma é para o envio de mensagens, então o sistema da Eventu através do \textit{Cloud Messaging}, permite que administradores do evento notifiquem usuários para promover novas interações e a retenção dos mesmos, como mostrado na Figura~\ref{fig-firebase-message-push}.

\begin{figure}[H]
\centering
\includegraphics[width=0.4\textwidth]{figuras/implementacao/push-notification.PNG}
\caption{Exemplo de Push Notification}
\label{fig-firebase-message-push}
\end{figure}

O serviço de \textit{Push Notification} fornecido pela Apple é o \textit{APNs (Apple Push Notification service)}. Como forma de centralizar o serviço, facilitando o envio tanto para iOS quanto Android, utilizamos o \textit{provider} fornecido pela Google que, ao identificar para qual plataforma ele deve enviar o \textit{push}, chama o serviço específico.

%%% Início de seção %%%
\section{Apresentação do sistema}
\label{sec-sistema}

Nesta seção, será apresentado o sistema por meio de uma série de capturas de tela. A Figura~\ref{fig-sistema-init} mostra ícone do aplicativo e a primeira tela de inicialização. Esses dois conteúdos são personalizáveis através do esquema de \textit{targets}, cada evento possui seu próprio \textit{target}.

\begin{figure}[H]
\centering
\includegraphics[width=0.3\textwidth]{figuras/sistema/sistema-icone.png}
\includegraphics[width=0.3\textwidth]{figuras/sistema/sistema-splash.png}
\caption{Inicialização do App}
\label{fig-sistema-init}
\end{figure}

A navegação principal do App é feita através da \textit{TabBar}. A Figura~\ref{fig-sistema-tab-home} apresenta a seção inicial, ela é descrita com o nome do evento, exibe todas atividades que ocorrerão no dia selecionado na aba superior, ordenadas por hora de início. Fazem parte da interface título, local, horário e cor, sendo que as cores servem para criar categorias visuais das atividades.

\begin{figure}[H]
\centering
\includegraphics[width=0.3\textwidth]{figuras/sistema/sistema-home.png}
\caption{\textit{Home}}
\label{fig-sistema-tab-home}
\end{figure}

Ao clicar em uma atividade na \textit{home} é exibida uma tela de detalhes, customizável, sendo responsabilidade do \textit{backend} exibir e preencher as seções. As únicas que obrigatoriamente devem aparecer são título e horário. A Figura~\ref{fig-sistema-detalhes} apresenta a tela de detalhes, no topo estão localizados os botões de ação para favoritar e adicionar na agenda, sua visibilidade é condicionada pelo \textit{remote config} e pelo \textit{response} do \textit{backend}, também armazenam a última opção selecionada. A tela pode possuir um \textit{header} com \textit{scroll} vertical de imagens. A primeira seção contém o título e descrição do evento. A segunda é destinada para exibir os palestrantes do evento. Na seção de horário é possível definir um lembrete para atividade. Depois temos uma seção de informações genéricas com título e descrição. A quinta seção traz informações gerais como duração pré requisitos e idioma da atividade. Por último temos as seções de interação com usuário.

\begin{figure}[H]
\centering
\includegraphics[width=0.3\textwidth]{figuras/sistema/sistema-detalhes-tipo1.png}
\includegraphics[width=0.3\textwidth]{figuras/sistema/sistema-detalhes-tipo2.png}
\includegraphics[width=0.3\textwidth]{figuras/sistema/sistema-detalhes-tipo3.png}
\caption{Detalhes Atividade}
\label{fig-sistema-detalhes}
\end{figure}

Avaliação e Perguntas dependem que o usuário esteja autenticado para prosseguir, caso contrário é sugerido o fluxo de login, através de um alerta, conforme mostrado na Figura~\ref{fig-sistema-sugestao-login}.

\begin{figure}[H]
\centering
\includegraphics[width=0.3\textwidth]{figuras/sistema/sistema-auth.png}
\caption{Sugestão Login}
\label{fig-sistema-sugestao-login}
\end{figure}

Os 3 itens exibidos na Figura~\ref{fig-sistema-acoes-user}, representam interações diretas com usuário seja fazendo uma pergunta, avaliando a atividade ou fazendo o download do material.

\begin{figure}[H]
\centering
\includegraphics[width=0.3\textwidth]{figuras/sistema/sistema-download.png}
\includegraphics[width=0.3\textwidth]{figuras/sistema/sistema-pergunta.png}
\includegraphics[width=0.3\textwidth]{figuras/sistema/sistema-avaliacao.png}
\caption{Interações com o usuário}
\label{fig-sistema-acoes-user}
\end{figure}

Os filtros por categorias de atividade, mostrado na Figura~\ref{fig-sistema-filtro}, são acessados através da \textit{home}, ao selecionar e aplicar um conjunto de itens, a lista principal de atividades é modificada, também é exibido a quantidade de categorias que foram escolhidas à direita do botão Filtros.

\begin{figure}[H]
\centering
\includegraphics[width=0.3\textwidth]{figuras/sistema/sistema-filtro.png}
\includegraphics[width=0.3\textwidth]{figuras/sistema/sistema-filtro-home.png}
\caption{Filtro de atividade}
\label{fig-sistema-filtro}
\end{figure}

A Figura~\ref{fig-sistema-mapa} apresenta a aba Mapa, responsável por listar todas as localidades do evento. Possui uma série de filtros, sendo possível também pesquisar por nome, alterando a lista interna de locais. Ao selecionar um item da lista interna o mapa se move até a região desejada. O filtro de categoria, com \textit{scroll} vertical, altera a visualização externa do mapa e a lista interna.

\begin{figure}[H]
\centering
\includegraphics[width=0.3\textwidth]{figuras/sistema/sistema-mapa-tipo1.png}
\includegraphics[width=0.3\textwidth]{figuras/sistema/sistema-mapa-tipo2.png}
\includegraphics[width=0.3\textwidth]{figuras/sistema/sistema-mapa-tipo3.png}
\caption{Mapa}
\label{fig-sistema-mapa}
\end{figure}

A Figura~\ref{fig-sistema-tab-palestrantes} representa a aba Palestrantes, onde são listados todos os palestrantes do evento, incluindo nome, foto, área de trabalho. Ao clicar em um item, uma descrição mais detalhada é fornecida.

\begin{figure}[H]
\centering
\includegraphics[width=0.3\textwidth]{figuras/sistema/sistema-palestrantes-close.png}
\includegraphics[width=0.3\textwidth]{figuras/sistema/sistema-palestrante-open.png}
\caption{Palestrantes}
\label{fig-sistema-tab-palestrantes}
\end{figure}

A Figura~\ref{fig-sistema-tab-notificacao} exibe a aba de Notificações, é uma área destinada aos administradores do evento para publicar novidades, informações, avisos. Podem existir 3 tipos de configurações de mensagem: texto mais imagem, somente imagem, somente texto. É uma lista paginada, conforme o \textit{scroll} chega ao fim uma nova requisição é feita pedindo mais itens.

\begin{figure}[H]
\centering
\includegraphics[width=0.3\textwidth]{figuras/sistema/sistema-notify-tipo1.png}
\includegraphics[width=0.3\textwidth]{figuras/sistema/sistema-notify-tipo2.png}
\caption{Notificações}
\label{fig-sistema-tab-notificacao}
\end{figure}

A aba de Ajustes, conforme mostra a Figura~\ref{fig-sistema-tab-ajustes}, provê a entrada para diversos outros fluxos, além de exercer o controle sobre o perfil do usuário, algumas seções como Sair, Credenciamento, Informações sobre a conta, só aparecem caso o login tenha sido realizado. Se o usuário não tiver se identificado aparecerá uma seção para o login.

\begin{figure}[H]
\centering
\includegraphics[width=0.3\textwidth]{figuras/sistema/sistema-ajustes-login.png}
\includegraphics[width=0.3\textwidth]{figuras/sistema/sistema-ajustes-logout.png}
\caption{Ajustes}
\label{fig-sistema-tab-ajustes}
\end{figure}

A Figura~\ref{fig-sistema-ajustes-login} apresenta a tela de login, responsável pela autenticação do usuário via e-mail e senha ou pelo provedor do Google, além de ter a opção recuperar senha e entrada para o fluxo de cadastro.

\begin{figure}[H]
\centering
\includegraphics[width=0.3\textwidth]{figuras/sistema/sistema-login-tipo1.png}
\includegraphics[width=0.3\textwidth]{figuras/sistema/sistema-login-tipo2.png}
\includegraphics[width=0.3\textwidth]{figuras/sistema/sistema-login-tipo3.png}
\caption{Login}
\label{fig-sistema-ajustes-login}
\end{figure}

O cliente possui a opção de permitir ou não o cadastro de um usuário pelo App, caso o cadastro esteja ativo ele pode ser feito via provedor do Google ou inserindo os dados, como demonstrado na Figura~\ref{fig-sistema-ajustes-login-cadastro}. Se estiver desabilitado ao clicar em se cadastrar na tela de login ele será direcionado para um site definido pelo cliente.

\begin{figure}[H]
\centering
\includegraphics[width=0.3\textwidth]{figuras/sistema/sistema-login-cadastro.png}
\caption{Cadastro}
\label{fig-sistema-ajustes-login-cadastro}
\end{figure}

A Figura~\ref{fig-sistema-ajustes-acao} exibe as tela de Favoritos e Meu Horário, ambas listam atividades adicionadas nesses tipos específicos pela tela de descrição de atividade, apresentada na Figura~\ref{fig-sistema-detalhes}. O Meu Horário também exibe atividades cadastradas pelo usuário no site do evento. Ambas as seções têm a visibilidade controladas por \textit{remote config} e aparecerem mesmo sem efetuar o login. Ao clicar em um item é aberta a tela de detalhes da atividade.

\begin{figure}[H]
\centering
\includegraphics[width=0.3\textwidth]{figuras/sistema/sistema-favorito.png}
\includegraphics[width=0.3\textwidth]{figuras/sistema/sistema-calendario.png}
\caption{Favoritos e Meu Horário}
\label{fig-sistema-ajustes-acao}
\end{figure}

A tela de Patrocinadores, exibida na Figura~\ref{fig-sistema-tab-ajustes}, lista os patrocinadores do evento divididos por cotas ou categorias. Possuem a visibilidade controlada pelo \textit{Remote Config}.

\begin{figure}[H]
\centering
\includegraphics[width=0.3\textwidth]{figuras/sistema/sistema-ajuste-patrocinio.png}
\caption{Patrocinadores}
\label{fig-sistema-ajustes-patrocinadores}
\end{figure}

A Figura~\ref{fig-sistema-ajustes-credenciamento} mostra a tela de Credenciamento, que só é exibida caso o usuário esteja autenticado, cadastrado no evento e o \textit{remote config} habilitado. Ele informa sobre o número do ingresso e exibe um QRCode deste número, para controle e gerência interna do evento.

\begin{figure}[H]
\centering
\includegraphics[width=0.3\textwidth]{figuras/sistema/sistema-ajuste-numero.png}
\caption{Credenciamento}
\label{fig-sistema-ajustes-credenciamento}
\end{figure}

Por fim, Termos de Uso e Ajuda direcionam o usuário para páginas Web com conteúdos específico fornecidos pelo cliente.