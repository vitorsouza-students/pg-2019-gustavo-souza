% ==============================================================================
% TCC - Gustavo Storck Andrade de Souza
% Capítulo 4 - Apoio ao desenvolvimento
% ==============================================================================
\chapter{Apoio ao Desenvolvimento}
\label{sec-apoio}

Neste capítulo, apresentamos os conceitos e implementações de ferramentas que nos ajudaram a economizar tempo e manter a qualidade no processo de desenvolvimento. A Seção~\ref{sec-padrao-codigo} discute sobre a importância de se adotar um padrão de código e exibe a ferramenta que adotamos para assegurar isso. Na Seção~\ref{sec-template} mostramos o \textit{template} desenvolvido para evitar o processo burocrático da criação de vários arquivos para compor uma cena. A Seção~\ref{sec-projects} relata como utilizamos e automatizamos o \textit{Git Project} para gerenciamento das tarefas. A Seção~\ref{sec-req-gest} apresenta a nossa adaptação do Scrum utilizando o \textit{Kanban} para auxiliar na gerência das atividades. Na Seção~\ref{sec-targets} apresentamos como utilizamos os \textit{Targets} para resolver o problema de adicionar e gerenciar vários clientes em uma mesma base de código. Na Seção~\ref{sec-CX} discutimos a ferramenta de CI e CD e como ela foi utilizada no processo de desenvolvimento e publicação. Por fim, na Seção~\ref{sec-resume-tools} apresentamos um resumo de todas ferramentas utilizadas no projeto assim como uma breve descrição do seu uso.

%%% Início de seção %%%
\section{Padrão de Código}
\label{sec-padrao-codigo}

Um padrão de código consiste em definir regras e estilos para escrita de código, que devem ser adotados por todo o time a fim de manter a consistência em todas partes do projeto como, por exemplo, declarar primeiro os atributos depois as funções, inserir um espaço entre o nome da variável e seu tipo, etc. Padrões de código são muito importantes, pois programar é uma maneira de se comunicar, uma forma de se expressar, deixar um registro que muitas vezes será dividido com outras pessoas, com o compromisso de entender e evoluir a partir do último ponto. Definir padrões favorece uma comunicação rápida e eficaz, a partir desse momento a contribuição e entendimento se tornam mais simples. 

Para implementar e assegurar os padrões utilizamos como ferramenta o \textit{SwiftLint}.\footnote{\url{https://github.com/realm/SwiftLint}} Na sua configuração, ele possui um arquivo no qual é possível definir os conjuntos de regras e em quais \textit{paths} elas se aplicam, a verificação é feita a partir da execução de um script que aplica todas regras aos arquivos adicionados e gera como saída \textit{warnings} ou erros conforme o configurado. Para automatizar a verificação adicionamos a execução do script como um dos passos do \textit{build} do Xcode de modo a gerar \textit{warnings} ou erros para cada violação das regras de forma visual para o programador. 

Outro ponto onde utilizamos o \textit{SwiftLint} foi como um dos processos de verificação obrigatórios do GitHub\footnote{\url{https://github.com}} para autorizar o \textit{merge} do \textit{Pull Request}. Nessa etapa o script é executado pelo CI e, se algum erro for encontrado, ele é apontado como um comentário e o \textit{merge} da \textit{branch} é bloqueado até que as pendências sejam resolvidas. Dessa forma conseguimos garantir a aplicação e consistência dos padrões em todo código.


\section{\textit{Template}}
\label{sec-template}

Quando definimos a arquitetura, nosso principal objetivo era garantir uma clara divisão de responsabilidade e desacoplamento entre as camadas e, para isso, utilizamos fortemente o conceito de inversão e injeção de dependências. Apesar de funcionar muito bem, há um preço a se pagar, um aumento na ``burocracia'' do código: por exemplo, para criar uma simples tela, são necessários no mínimo 7 arquivos, sendo o conjunto de todos arquivos necessários para se criar essa tela chamado de cena. Era extremamente maçante criar todos os arquivos e compor a injeção de dependência manualmente entre eles. 

Nossa solução foi automatizar essa tarefa desenvolvendo um \textit{template} para o \textit{Xcode} chamado VIP, que funciona de forma similar a um \textit{plugin} que pode ser baixado e instalado na IDE de qualquer membro da equipe. Após instalado, ele passa a ser listado em conjunto com as outras opções nativas já existentes.  A Figura~\ref{fig-template} ilustra seu uso: o desenvolvedor seleciona o \textit{template}, insere o nome base de todos os arquivos e escolhe o \textit{path}. Ao final do processo todos os sete arquivos contendo o código base da arquitetura de uma cena estarão disponíveis.

\begin{figure}[H]
\centering
\includegraphics[width=0.6\textwidth]{figuras/requisito/requisitos-template-passo-1.png}
\includegraphics[width=0.6\textwidth]{figuras/requisito/requisitos-template-passo-2.png}
\includegraphics[width=0.6\textwidth]{figuras/requisito/requisitos-template-passo-3.png}
\includegraphics[width=0.6\textwidth]{figuras/requisito/requisitos-template-final.png}
\caption{Passos para criar os arquivos}
\label{fig-template}
\end{figure}

\section{\textit{Git Project}}
\label{sec-projects}

O \textit{Git Project} foi escolhido como ferramenta de gerência para o desenvolvimento do projeto.

Todas as tarefas necessárias para o desenvolvimento do sistema foram cadastradas como \textit{issues}, mencionadas anteriormente na Seção~\ref{sec-tools-manager}. \textit{Issues} são entidades com título e descrição e que possuem uma seção que possibilita perguntas, discussões e comentários, além de ser possível atribuir alguns atributos como rótulo e responsável pela tarefa. Essas \textit{issues} são transformadas em cartões para facilitar a visualização dentro do quadro do \textit{Git Project}. 

O Quadro contém todas os estados do desenvolvimento e  foi automatizado para quando uma \textit{issue} fosse aberta, ela fosse adicionada à coluna de \textit{Product Backlog}. No início de cada semana as tarefas com maior prioridade  e que podem ser desenvolvidas dentro do \textit{time box} da \textit{sprint} são movidas para o \textit{To do}. A coluna de \textit{Done} também foi automatizada para mover um cartão quando o código relativo a uma determinada \textit{issue} for incorporado ao ramo de origem. Os demais estados exigem que progressão das tarefas seja feita manualmente pelo seu responsável.

%%% Início de seção %%%
\section{Fluxo de Gestão}
\label{sec-req-gest}

O \textit{Scrum} traz conceitos de gerência eficazes e flexíveis, podendo ser adaptados a vários contextos e tamanhos de empresas. Para o nosso projeto, foi levado em consideração que todos os papéis serão desempenhados por uma única pessoa, por isso não foram exercidos todos os passos do modelo tradicional e o fluxo foi adaptado para nossa realidade.

Nosso fluxo começa após a concepção do sistema, definindo o objetivo geral e funcionalidades pretendidas. Nesse ponto como \textit{Product Owner} fiz uma separação das tarefas macro: primeiro são divididas as funcionalidades e tarefas gerais do sistema, como login, camada de serviço, tela de palestrantes, etc. É interessante destacar que nem todas as tarefas serão definidas no início do projeto, podendo surgir novas tarefas ao decorrer do desenvolvimento, como uma alteração de escopo exigida pelo cliente (novas funcionalidades), ou tarefas para correção de erros.

Em seguida as tarefas macro são divididas em tarefas micro, que contém uma descrição mais técnica e uma estimativa em \textit{Story Points}. Cada \textit{Story Point} equivale a 1 hora e meia de trabalho e nossa \textit{Sprint} tem duração de 1 semana. Após a identificação e classificação das tarefas elas são cadastradas no \textit{Product Backlog}.  O \textit{Product Backlog} faz parte do nosso fluxo de desenvolvimento.

O fluxo de desenvolvimento consiste nas sequência de etapas a qual uma tarefa deve ou pode passar para se tornar efetivamente concluída, gerenciadas através do Git Project como um \textit{kanban}. Essas etapas são:
 
\begin{itemize}

\item \textbf{\textit{Product Backlog}}: Lista de todas tarefas necessárias para se cumprir o projeto

\item \textbf{\textit{To do}}: tarefas que foram atribuídas a um desenvolvedor na \textit{Sprint Planning Meeting}, mas ainda não começaram a ser realizadas;

\item \textbf{\textit{In progress}}: tarefas em desenvolvimento;

\item \textbf{\textit{On Hold}}: tarefas que tiveram algum impedimento, que impossibilitaram a continuação do desenvolvimento;

\item \textbf{\textit{Done}}: uma tarefa é considerada concluída quando ela passa por pelo menos \textit{In progress}, \textit{Code Review} e \textit{Test}.

\end{itemize}

Esse fluxo ajuda a assegurar a qualidade e transparência do desenvolvimento, além de dar visibilidade a cada tarefa.

\section{\textit{Targets}}
\label{sec-targets}

Todo projeto contém pelo menos um \textit{target}. Um \textit{target} especifica uma aplicação, define um nome, recursos dos dispositivos móveis que irão executar o código e os arquivos que o compõem. O Xcode permite criar múltiplos \textit{targets} pertencentes ao mesmo projeto. Isso é muito importante devido ao nosso modelo de negócios, pois nossa proposta é oferecer a mesma base de código para múltiplos clientes, fazendo personalizações simples. 

Na primeira versão do projeto mantínhamos um repositório para cada cliente, faziamos um \textit{fork} do código base e customizávamos para atender a demanda do evento. Ao longo do tempo esse tipo de abordagem ficou totalmente inviável, pois se descobríssemos um erro tínhamos que atualizar todos os repositórios, resultando em uma grande perda de tempo, pois o número de repositórios era enorme. 

Analisando melhor, percebemos que as atualizações se limitavam ao nome, logomarca, URL do servidor e escolha de funcionalidades. A escolha de funcionalidade poderíamos resolver utilizando o \textit{Remote Config}, porém tanto o \textit{Remote Config} quanto a publicação na \textit{AppStore} exigiam que cada aplicativo tivesse um identificador único, conhecido como \textit{BundleID}, o que nos motivou inicialmente a usar projetos distintos para cada evento. No entanto, com o uso dos \textit{targets} foi possível solucionar nosso problema: cada \textit{target} tem seu próprio \textit{BundleID} e pode sofrer \textit{deploy} separadamente. Podemos definir constantes para cada \textit{target} o que permite definir imagem, nome e URL específicas de cada evento. Hoje, para cada evento criamos um novo \textit{target}, definimos \textit{BundleID}, imagem, URL, e um novo projeto no Firebase, serviço responsável por gerenciar a autenticação, \textit{Remote Config}, logs de eventos e falhas. Tudo se encontra dentro do mesmo projeto e repositório, com todos arquivos fontes compartilhados por todos os eventos. A Figura~\ref{fig-targets} mostra uma lista de \textit{targets} do projeto.

\begin{figure}[H]
\centering
\includegraphics[width=0.5\textwidth]{figuras/requisito/requisitos-target.png}
\caption{Diferentes Targets}
\label{fig-targets}
\end{figure}

\section{CI e CD}
\label{sec-CX}

CI e CD são ferramentas fundamentais que compõem nossa esteira de desenvolvimento e \textit{deploy}. No projeto foram configurados 3 \textit{workflows}.

\begin{itemize}

\item \textbf{\textit{developer}}: é executado ao abrir um \textit{pull request} de qualquer \textit{branch} para \textit{developer}, sendo responsável por fazer o \textit{buid} do App, rodar os testes unitários e verificar o padrão de código. Depois de ter um \textit{pull request} aberto, qualquer novo \textit{push} executa o fluxo novamente;

\item \textbf{\textit{master}}: tem as mesmas etapas da \textit{developer}, mas é executada ao abrir um \textit{pull request} da \textit{developer} para \textit{master};

\item \textbf{\textit{deploy}}: executa ao se criar uma nova Tag para o projeto, além de executar todas verificações feitas pela \textit{developer}, envia e publica o aplicativo na \textit{App Store}.

\end{itemize}

Os fluxos de \textit{developer} e \textit{master} fazem parte do CI, e estão integrados com o GitHub. Habilitam o botão de \textit{merge} caso todos os passos sejam executados com sucesso, como mostra a Figura~\ref{fig-cx-gihub_merge}.
O fluxo de \textit{deploy} caracteriza o CD, automatizando todo o processo de publicação do App.
Para compor esses fluxos foram utilizadas 3 ferramentas.

\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{figuras/requisito/requisitos-cx-git.png}
\caption{Status do Github ao aguardar as verificações do CI.}
\label{fig-cx-gihub_merge}
\end{figure}


\begin{itemize}

\item \textbf{\textit{Fastlane}}: é uma ferramenta gratuita, \textit{open source}, com diversos \textit{plugins} escritos pela comunidade. Foi utilizado para executar os testes e fazer o processo de publicação do aplicativo. Uma das características mais interessantes para o nosso projeto é que ele não depende de estar em um serviço externo para ser executado: depois de configurado, podemos rodar as \textit{lanes} localmente. A Listagem~\ref{codigo_cx_fastlane} exibe o arquivo de configuração do \textit{Fastlane};

\lstinputlisting[label={codigo_cx_fastlane}, caption={Arquivo de configuração Fastlane}, language={Ruby}, float={htpb}]{codigos/codigo_cx_fastlane.ruby}

\item \textbf{\textit{Danger}}: é uma ferramenta gratuita, \textit{open source}, responsável por automatizar as convenções da sua equipe. No projeto, ele verifica o tamanho do \textit{Pull Request}, limitado a 500 linhas de adição de código, e faz a análise do padrão de código utilizando o \textit{Linter}. Caso algum problema seja encontrado ele comenta diretamente trecho de código correspondente pelo GitHub. A Figura~\ref{fig-cx-danger} mostra um comentário feito pelo Danger após ser executado pelo CI e encontrar uma divergência no padrão de código;

\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{figuras/requisito/requisitos-cx-danger.png}
\caption{Comentário \textit{Danger}}
\label{fig-cx-danger}
\end{figure}

\item \textbf{\textit{Bitrise}}: é um Serviço externo de CI/CD pago, ele se conecta ao repositório, define os \textit{triggers}, que determinam qual \textit{workflow} será executado quando determinada ação ocorrer, além de habilitar ou não o processo de \textit{merge}. Executa o \textit{Fastlane} e o \textit{Danger} para implementar as ações de teste, análise de código e publicação. A Figura~\ref{fig-cx-bitrise}, exibe o progresso da execução do CI, iniciado após um \textit{pull request} para \textit{developer}. 

\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{figuras/requisito/requisitos-cx-bitrise.png}
\caption{\textit{Status Bitrise}}
\label{fig-cx-bitrise}
\end{figure}

\end{itemize}

\section{Resumo das Ferramentas}
\label{sec-resume-tools}
A Tabela~\ref{tabela-apoio-tools} lista todas as ferramentas descritas neste capítulo, resumindo em uma breve descrição para que foram utilizadas no contexto deste trabalho.

%%% Tabela de Ferramentas - Inicio %%%
\newpage
\begin{table}
\caption{Tabela de Ferramentas usadas no projeto.}
\label{tabela-apoio-tools}
\centering
\renewcommand{\arraystretch}{1.2}
\begin{small}
\begin{tabular}{| p{0.4\linewidth} | p{0.5\linewidth} |}\hline
\textbf{\textit{Nome}} & \textbf{Uso}\\\hline

Astah \url{https://astah.net/}
&
Ferramenta utilizada para modelagem UML e de dependências.
\\\hline

Figma \url{https://www.figma.com/}
&
Ferramenta de \textit{design} para prototipação das telas e dos fluxos.
\\\hline

GitHub Project \url{https://github.com/features/project-management/}
&
Ferramenta para gerência de projeto, usado para registrar as tarefas e como Kanban.
\\\hline

GitHub \url{https://github.com/}
&
Ferramenta para controle de versão do código.
\\\hline

Fork \url{https://git-fork.com/}
&
Cliente \textit{desktop} para Git.
\\\hline

Xcode \url{https://developer.apple.com/xcode/ide/}
&
Ambiente de desenvolvimento para ecossistema Apple.
\\\hline

Proxyman \url{https://proxyman.io/}
&
\textit{Proxy} de depuração Web, para mudar as respostas do servidor. Usada nos testes exploratórios.
\\\hline

CocoaPods \url{https://cocoapods.org/}
&
Gerenciador de dependências para projetos Swift e Objective-C.
\\\hline

SwiftLint \url{https://github.com/realm/SwiftLint}
&
Ferramenta para aplicar o estilo e as convenções do Swift.
\\\hline

Fastlane \url{https://fastlane.tools/}
&
Ferramenta para automatizar o processo de desenvolvimento e publicação
\\\hline

Danger \url{https://danger.systems/swift/}
&
Automatiza tarefas de revisão de código no CI.
\\\hline

Bitrise \url{https://www.bitrise.io/}
&
Ferramenta de CI/CD.
\\\hline

\end{tabular}
\end{small}
\end{table}

%%% Tabela de Ferramentas - Fim %%%