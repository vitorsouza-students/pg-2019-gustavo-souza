% ==============================================================================
% TCC - Gustavo Storck Andrade de Souza
% Capítulo 2 - Referencial Teórico
% ==============================================================================
\chapter{Fundamentação Teórica e Tecnologias Utilizadas}
\label{sec-referencia}

Neste capítulo analisamos os métodos e tecnologias usadas como base para realização deste trabalho. A Seção~\ref{sec-plataform} analisa as vantagens e desvantagens das diferentes abordagens para desenvolvimento de aplicativos \textit{mobile}, justificando a escolha da abordagem utilizada neste trabalho; a Seção~\ref{sec-architeture} relata tarefa análoga, porém em relação às diferentes arquiteturas utilizadas em projetos para esta plataforma; a Seção~\ref{sec-referencia-testes} discute sobre a importância e os principais tipos de testes; a Seção~\ref{sec-referencia-Cx} apresenta os conceitos de CI e CD; a Seção~\ref{sec-referencia-views} discorre sobre as 3 principais formas de se implementar interfaces gráficas no iOS; a Seção~\ref{sec-ref-clean} introduz os conceitos de \textit{Clean Code} e \textit{Clean Architecture}; a Seção~\ref{sec-Scrum} apresenta o \textit{Scrum}; e, finalmente, a Seção~\ref{sec-tools} menciona as ferramentas de apoio para realização deste fluxo de trabalho.

%%% Início de seção %%%
\section{Abordagens}
\label{sec-plataform}

Um requisito fundamental para Eventu é que o aplicativo execute na plataforma iOS. iOS é o sistema operacional \textit{mobile} usado pelos iPhones, desenvolvido pela Apple e apresentado no ano de 2007. A empresa adota uma medida protecionista de mercado onde ela só permite que seus dispositivos \textit{mobile} executem o sistema operacional próprio. Para desenvolver um aplicativo para iPhone é necessário que ele seja desenvolvido para iOS e distribuído pela AppStore.\footnote{\url{https://www.apple.com/ios/app-store/}}

Hoje no mercado existem três principais abordagens para desenvolvimento de software para esta plataforma: Nativo, Híbrido e \textit{Cross Platform}. Esta seção destina-se a apresentar cada uma das alternativas, mostrando seus pontos fortes e fracos. A escolha da plataforma é essencial pois impacta no desenvolvimento e resultado do produto.

%%% Início de seção. %%%
\subsection{Nativo}
\label{sec-plataform-native}
São aplicativos desenvolvidos para uma plataforma específica, nas linguagens aceitas por essa plataforma. Por esse motivo acabam tendo acesso direto às \textit{frameworks} nativas que fazem a interface com o sistema operacional. Para o iOS temos uma arquitetura em camadas, ilustrada na Figura~\ref{fig-arquitetura-ios}.

\begin{figure}[H]
\centering
\includegraphics[width=.50\textwidth]{figuras/referencial/arquitetura-ios.png} 
\caption{Arquitetura em camadas iOS\protect\footnotemark}
\label{fig-arquitetura-ios}
\end{figure}
\footnotetext{\url{https://developer.apple.com/library/archive/documentation/MacOSX/Conceptual/OSX_Technology_Overview/About/About.html}}

\textbf{Cocoa Touch:}
é a principal responsável pela aparência do aplicativo e sua capacidade de resposta às ações do usuário, fornecendo a infraestrutura básica para um conjunto de tarefas chave como multitarefa e \textit{Auto Layout}. A principal \textit{framework} dessa camada é a \textit{UIKit}, responsável pelo suporte a aplicações gráficas orientadas a eventos. Ela fornece a janela e a arquitetura de visualização para implementar a interface e a infraestrutura de manipulação de eventos.

\textbf{Media:}
fornece os recursos de áudio, vídeo, animação e gráficos. Assim como acontece com as outras camadas, a camada de mídia compreende uma série de estruturas que podem ser utilizadas no desenvolvimento de aplicativos para iPhone.

\textbf{Core Service:}
fornece grande parte da base sobre a qual as camadas acima são construídas. Ele fornece armazenamento no iCloud, internacionalização, localização, proteção de dados, suporte a compartilhamento de arquivos, GCD (API para trabalhar com \textit{threads}).

\textbf{Core OS:}
essa camada é responsável pelo gerenciamento da memória, alocando e liberando memória, cuidando das tarefas do sistema de arquivos, do gerenciamento da rede e de outras tarefas do sistema operacional. Também interage diretamente com o hardware.

\textbf{Prós:}
\begin{itemize}

\item Fornece todos os recursos disponíveis no dispositivo e sistema operacional;

\item Alto desempenho e velocidade;

\item Usa tecnologias recomendadas e apoiadas pelos desenvolvedores do sistema operacional, no caso do iOS;

\item Oferece experiência de usuário compatível com o sistema operacional;

\item Segurança;

\item Desenvolvimento maduro e consolidado, que já está sendo evoluído e testado há anos, com grande apoio da comunidade e extensa documentação.

\end{itemize}

\textbf{Contras:}
\begin{itemize}

\item Os códigos não são compartilhados entre as diferentes plataformas de dispositivos móveis, logo é necessário manter um time de desenvolvedores para cada plataforma afetando o preço e tempo de desenvolvimento.

\end{itemize}

%%% Início de seção. %%%
\subsection{Híbrido}
\label{sec-plataform-hybrid}
Tecnologias híbridas permitem usar desenvolvimento Web (HTML, CSS, JavaScript) para criar aplicativos que executem em múltiplas plataformas (Android, iOS, \textit{desktop}, navegadores), usando a mesma base de código. Elas possuem bibliotecas que simulam elementos visuais e comportamentos nativos, são executadas dentro de contêineres, no \textit{Mobile Web Views}, por onde pode ser feita a integração com \textit{hardware}, como abrir a câmera. É muito similar a um site na Web, com a diferença que pode ser obtido e executado como um aplicativo no dispositivo móvel. Ferramentas como o Ionic\footnote{\url{https://ionicframework.com}} e o Apache Cordova\footnote{\url{https://cordova.apache.org}} permitem o desenvolvimento de aplicações utilizando esta abordagem.


\textbf{Prós:}
\begin{itemize}

\item Utiliza a mesma base de código, reduzindo as equipes e tempo de desenvolvimento;

\item Portabilidade, pode ser executado em múltiplas plataformas, iOS, Android, navegadores e \textit{desktops}.

\end{itemize}

\textbf{Contras:}
\begin{itemize}

\item Baixo desempenho;

\item Experiência do usuário incompleta;

\item Alta dependência de bibliotecas;

\item Dificuldade para integrar com funcionalidades do sistema operacional, como \textit{push notification}, por exemplo.

\end{itemize}

%%% Início de seção. %%%
\subsection{\textit{Cross Platform}}
\label{sec-plataform-cross}
Utiliza a mesma base de código para criar aplicativos que executem em múltiplas plataformas (ex.: Android, iOS). A maior diferença em relação aos aplicativos híbridos é que o \textit{Cross Platform} utiliza bibliotecas que traduzem o seu código em elementos visuais e comportamentos nativos de cada plataforma, além do aplicativo ser também executado de forma nativa, o que traz um grande ganho de desempenho e usabilidade em relação ao híbrido. Ferramentas como o React Native,\footnote{\url{https://facebook.github.io/react-native/}} o Xamarin\footnote{\url{https://visualstudio.microsoft.com/xamarin}} e o Flutter\footnote{\url{https://flutter.dev}} permitem o desenvolvimento de aplicações utilizando esta abordagem.


\textbf{Prós:}
\begin{itemize}

\item Utiliza a mesma base de código, reduzindo as equipes e tempo de desenvolvimento;

\item Portabilidade, pode ser executado em múltiplas plataformas como, por exemplo, iOS e Android.

\end{itemize}

\textbf{Contras:}
\begin{itemize}

\item Médio desempenho;

\item Experiência do usuário incompleta;

\item Personalização dependente da \textit{framework};

\item Pela grande dependência de bibliotecas, o \textit{build} final fica maior que o nativo.

\end{itemize}

\subsection{Conclusões}
\label{sec-plataform-conclusao}
Nosso projeto foi desenvolvido de maneira nativa, por oferecer a melhor experiência do usuário com base na velocidade, conformidade da plataforma e uso dos recursos mais recentes. A Apple continua investindo na abordagem nativa, criando novas tecnologias, como o SwiftUI\footnote{\url{https://developer.apple.com/xcode/swiftui/}} e melhorando as existentes, como Xcode\footnote{\url{https://developer.apple.com/xcode/}} e Swift\footnote{\url{https://swift.org}}, otimizando a compilação, melhorando tempo de execução, adicionando mais funcionalidades às linguagens, com excelente suporte, documentação e envolvimento da comunidade.


\subsection{Linguagem de Programação}
\label{sec-plataform-language}
Para o desenvolvimento nativo na plataforma iOS, pode-se utilizar como linguagem de programação Objective-C ou Swift~\cite{iOS-dev}.

O Objective-C foi criado no início da década de 1980 por Brad Cox e Tom Love como uma extensão do C. Fornece recursos orientados a objetos e um tempo de execução dinâmico. O Objective-C herda a sintaxe, os tipos primitivos e as instruções de controle de fluxo de C e adiciona a sintaxe para definir classes e métodos. No entanto, atualmente é uma linguagem com características ultrapassadas, por exemplo: ainda existem ponteiros explícitos, alocação de memória, arquivo de cabeçalho (\texttt{.h}), código muito verboso, não apresenta elementos funcionais, não possui  \textit{Null Safety}. 

Observando a defasagem e os crescentes problemas com a linguagem, a Apple em 2014 resolveu atender a comunidade e lançou o Swift, uma linguagem orientada a objetos, estruturada, imperativa, compilada, concorrente e funcional. Além disso, a linguagem ainda mantém compatibilidade com código existente em Objective-C. 

Apesar da Apple dar suporte às duas linguagens, para este projeto adotamos o Swift, por ser mais moderna, segura e fácil de aprender.

%%% Início de seção. %%%
\section{Arquiteturas}
\label{sec-architeture}

Nessa seção discutimos os principais padrões de arquiteturas utilizadas no desenvolvimento de aplicativos \textit{mobile}. A escolha de uma arquitetura correta implica em melhor isolamento do código, melhor escalabilidade, facilitando a manutenção e aplicação de testes.

%%% Início de seção. %%%
\subsection{MVC (\textit{Model-View-Controller})}
\label{sec-architeture-mvc}

\begin{figure}[H]
\centering
\includegraphics[width=.80\textwidth]{figuras/referencial/arquitetura-mvc.png} 
\caption{Arquitetura MVC}
\label{fig-arquitetura-mvc}
\end{figure}

O padrão MVC\footnote{\url{https://developer.apple.com/library/archive/documentation/General/Conceptual/DevPedia-CocoaCore/MVC.html}} (\textit{Model-View-Controller}), ilustrado na Figura~\ref{fig-arquitetura-mvc}, é o padrão mais simples e foi durante muitos anos o recomendado pela Apple. Nessa arquitetura, a \textit{View} é responsável pela apresentação da interface, e notifica o \textit{Controller} sobre as ações do usuário como, por exemplo, o clique de um botão. O \textit{Model} representa os dados do seu domínio, por exemplo uma classe \textsf{Filme}. O \textit{Controller} tem a função de atualizar a \textit{View} e o \textit{Model}, garantindo a comunicação entre os dois. A \textit{View} e o \textit{Model} não conhecem o \textit{Controller}, elas notificam suas mudanças de estados e esperam alguém reagir a isso.

\begin{figure}[H]
\centering
\includegraphics[width=.80\textwidth]{figuras/referencial/arquitetura-mvc-realidade.png} 
\caption{Arquitetura MVC na realidade.}
\label{fig-arquitetura-mvc-realidade}
\end{figure}

Vale destacar que o MVC no desenvolvimento \textit{mobile} é adaptado às características desta plataforma, portanto se distingue do MVC tradicional ilustrado anteriormente.  Uma representação mais fiel dessa arquitetura no desenvolvimento iOS seria a Figura~\ref{fig-arquitetura-mvc-realidade},\footnote{\url{https://engineering.etermax.com/dealing-with-massive-view-models-using-mvvm-on-ios-74b2697557ce}} na qual percebemos como \textit{View} e \textit{Controller} são fortemente conectados pelo ciclo de vida. Em um padrão MVC isso é um problema, pois força o controlador a lidar com inúmeras responsabilidades. Os principais problemas dessa abordagem é a dificuldade de realizar testes unitários e a precária divisão de responsabilidade, ferindo os princípios da responsabilidade única.

\subsection{MVP (\textit{Model-View-Presenter})}
\label{sec-architeture-mvp}

\begin{figure}[H]
\centering
\includegraphics[width=.80\textwidth]{figuras/referencial/arquitetura-mvp.png} 
\caption{Arquitetura MVP.}
\label{fig-arquitetura-mvp}
\end{figure}

O MVP\footnote{\url{https://medium.com/@saad.eloulladi/ios-swift-mvp-architecture-pattern-a2b0c2d310a3}} (\textit{Model-View-Presenter}), ilustrado na Figura~\ref{fig-arquitetura-mvp}, é uma extensão do MVC que trata o \textit{Controller} como parte da \textit{View}, distribuindo sua responsabilidade com o \textit{Presenter}. A \textit{View} continua com a função de lidar com a interface do usuário e o \textit{Model} com o domínio, enquanto o \textit{Presenter} trata da lógica de negócios e apresentação. Dessa forma fica mais fácil de aplicar testes unitários, pois toda lógica de negócio está isolada e não depende do ciclo de vida da \textit{View}. Em relação ao MVC, ele distribui melhor as responsabilidades, o que diminui o custo de manutenção e melhora a escalabilidade.

\subsection{MVVM (\textit{Model-View-ViewModel})}
\label{sec-architeture-mvvm}

\begin{figure}[H]
\centering
\includegraphics[width=.80\textwidth]{figuras/referencial/arquitetura-mvvm.png} 
\caption{Arquitetura MVVM}
\label{fig-arquitetura-mvvm}
\end{figure}

O MVVM\footnote{\url{https://www.appcoda.com/mvvm-vs-mvc/}} (\textit{Model-View-ViewModel}), ilustrado na Figura~\ref{fig-arquitetura-mvvm}, foi apresentado pela Microsoft em 2005 para facilitar a programação orientada a eventos e desde então vem sendo amplamente adotado no desenvolvimento \textit{mobile}. O \textit{Model} e a \textit{View} continuam sendo os mesmos que no MVP, mas aqui o intermediário \textit{View Model}, atualiza a \textit{View} usando \textit{data binding}. Ao contrário do \textit{Presenter}, que realiza chamadas diretas a \textit{View}, o \textit{View Model}, expõe eventos de mudança que são utilizados pelo \textit{Controller} para alterar a interface.

\subsection{MVVM-C (\textit{Model-View-ViewModel-Coordinator})}
\label{sec-architeture-mvvmc}

\begin{figure}[H]
\centering
\includegraphics[width=.80\textwidth]{figuras/referencial/arquitetura-mvvmc.png} 
\caption{Arquitetura MVVM-C.}
\label{fig-arquitetura-mvvmc}
\end{figure}

O MVVM-C\footnote{\url{https://medium.com/sudo-by-icalia-labs/ios-architecture-mvvm-c-introduction-1-6-815204248518/}} (\textit{Model-View-ViewModel-Coordinator}), ilustrado na Figura~\ref{fig-arquitetura-mvvmc}, introduz um novo componente ao MVVM tradicional, o \textit{Coordinator}, que nos permite fazer o controle do fluxo das telas. Além disso, ele torna o \textit{Controller} ainda mais passivo tirando a responsabilidade de orquestrar o fluxo das telas e fazer a injeção de dependência. Até então nas arquiteturas anteriores era dever do \textit{Controller} instanciar, adicionar e retirar da pilha de navegação outro \textit{Controller}, o que cria dois problemas. Primeiro, em uma arquitetura que preza o isolamento uma \textit{View} não deveria saber em qual parte do fluxo está inserida e muito menos as regras de negócio para dar continuidade. O \textit{Controller} deve se preocupar somente em responder às ações da sua interface. O segundo problema que o \textit{Coordinator} resolve é a comunicação entre as telas. Imagine o seguinte caso: temos algumas telas na nossa pilha de navegação que representam um fluxo de login. Ao final do login temos que notificar a segunda tela da pilha que tudo foi realizado com sucesso. Nesse caso o último \textit{Controller} não conhece a segunda tela e sem alguém responsável por coordenar o fluxo teríamos um problema. O MVVM-C usa as vantagens conhecidas do MVVM e fornece uma solução para o problema de rotas no desenvolvimento \textit{mobile}.

\subsection{VIPER (\textit{View-Interactor-Presenter-Entity-Router})}
\label{sec-architeture-viper}

O VIPER\footnote{\url{https://www.raywenderlich.com/8440907-getting-started-with-the-viper-architecture-pattern}} (\textit{View-Interactor-Presenter-Entity-Router}), ilustrado na Figura~\ref{fig-arquitetura-viper}, eleva a divisão de responsabilidades e também se preocupa com o roteamento das telas, fazendo um fluxo bidirecional dos dados. Nesta arquitetura existem cinco camadas:

\begin{figure}[H]
\centering
\includegraphics[width=.80\textwidth]{figuras/referencial/arquitetura-viper.png} 
\caption{Arquitetura Viper.}
\label{fig-arquitetura-viper}
\end{figure}

\begin{itemize}

\item \textbf{\textit{Interactor}}: contém a lógica de negócio relacionada aos dados,

\item \textbf{\textit{Presenter}}: contém a lógica de negócios da UI, chama métodos do \textit{Interactor};

\item \textbf{\textit{Entity}}: objetos de domínio, são usados apenas para representar e armazenar, todo tratamento de dados é feito pelo \textit{Interactor};

\item \textbf{\textit{Router}}: responsável pela navegação entre telas, muito parecido com o \textit{coordinator} do MVVM-C;

\item \textbf{\textit{View}}: continua a mesma das outras arquiteturas, sendo compostas pela \textit{View} e pelo \textit{Controller}.

\end{itemize}

O VIPER se destaca com uma proposta de arquitetura muito bem definida, onde cada camada possui uma única responsabilidade.

\subsection{Clean Swift ou VIP (\textit{View-Interactor-Presenter})}
\label{sec-architeture-vip}

\begin{figure}[H]
\centering
\includegraphics[width=.80\textwidth]{figuras/referencial/arquitetura-vip.png} 
\caption{Arquitetura Vip.}
\label{fig-arquitetura-vip}
\end{figure}

O VIP\footnote{\url{https://clean-swift.com/}} (\textit{View-Interactor-Presenter}), ilustrado na Figura~\ref{fig-arquitetura-vip}, segue a mesma divisão de responsabilidades do VIPER, mas define menos atores na arquitetura. Sua principal vantagem quanto ao VIPER é que ele apresenta um fluxo unidirecional de dados.

%%% Início de seção %%%
\section{Testes}
\label{sec-referencia-testes}

Testes são importantes para evitar que mudanças alterem comportamentos definidos anteriormente de forma inesperada. A implementação dos testes é feita em conjunto com a sua tarefa, logo uma tarefa compreende resolver um determinado problema e escrever testes para essa solução. 

Um dos conceitos mais bem difundidos sobre teste é a Pirâmide de Testes,\footnote{\url{https://medium.com/pacroy/separate-unit-integration-and-functional-tests-for-continuous-delivery-f4dc240d8f2f}} que divide os testes em 3 categorias, conforme mostra Figura~\ref{fig-testes-piramide}:

\begin{figure}[H]
\centering
\includegraphics[width=0.66\textwidth]{figuras/implementacao/teste_piramide.png}
\caption{Pirâmide de Teste}
\label{fig-testes-piramide}
\end{figure}

\begin{itemize}

\item \textbf{Unitário}: busca testar a menor unidade que temos no código a função. Ele analisa a saída para um conjunto de entradas e verifica se o resultado é o esperado. Garante, de forma automatizada, que a função não tenha seu comportamento alterado de forma acidental;

\item \textbf{Integração}: testam o funcionamento da união de componentes, podem existir situações em que os teste unitário validam 2 componentes de código como corretas, mas a integração entre as duas não surtir o resultado esperado. A integração testa o resultado da união desses componentes;

\item \textbf{E2E}: teste de ponta a ponta ou funcionais, procuram garantir que toda a aplicação, com suas funções e integrações, funciona como esperado. Para programação \textit{mobile} é muito comum utilizarmos teste de interface para fazermos essa verificação.

\end{itemize}

É interessante observar que ao subir na pirâmide os testes ficam computacionalmente mais caros de serem realizados, demandando mais tempo para serem executados, ao mesmo tempo o topo da pirâmide aproxima os testes para uma forma mais real da interação do usuário com aplicação. É um \textit{trade-off} entre tempo de desenvolvimento e veracidade do cenário de teste.

%%% Início de seção %%%
\section{CI e CD}
\label{sec-referencia-Cx}

CI (\textit{Continuous Integration}) e CD (\textit{Continuous Delivery}) são dois acrônimos que costumam ser mencionados quando as pessoas falam sobre práticas de desenvolvimento modernas. CI significa integração contínua: para cada tarefa concluída as alterações são validadas criando um \textit{build}, e executando os testes automatizados. Na nossa integração, uma tarefa só é considerada concluída caso esse processo seja feito com sucesso. CD significar entrega contínua, ele automatiza o processo de publicação do aplicativo na \textit{AppStore} e no \textit{TestFlight}, por meio dele podemos definir regras, como, em quantos tempo x\% da base receberá a atualização, quem tem acesso ao aplicativo em fase beta, etc.

%%% Início de seção %%%
\section{Views}
\label{sec-referencia-views}

Na programação para dispositivos iOS existem 2 formas de se construir uma interface: a primeira via \textit{interface builder} e a segunda via código, também conhecida com \textit{View Code}. Essa seção irá introduzir as duas formas.

%%% Início de seção. %%%
\subsection{Interface Builder}
\label{sec-referencia-views-interface}

\textit{Interface builder} é método visual de se implementar \textit{views}, ele consiste em selecionar os elementos e organizá-los de forma a compor uma interface. Eles são organizados através de \textit{constraints}, que definem ordem e prioridade por meio de âncoras: cada elemento deve estar ancorado em relação ao elemento pai ou a um elemento adjacente. É um método bem rápido e intuitivo de desenhar interfaces, que utiliza recurso de arrastar e soltar objetos, permitindo configurá-los por meio de uma janela de especificações. É possível fazer referência no código aos elementos gráficos por links simbólicos, chamados de \textit{IBoutlets}. Podem ser implementados de duas formas:

\begin{itemize}

\item \textbf{\textit{Xib}}: representa uma tela completa, ou um componente que pode ser reutilizado por outros \textit{Xibs};

\item \textbf{\textit{Storyboards}}: é um conjunto de \textit{Xibs}, tem o poder de representar o fluxo e fazer a navegação entre as telas.

\end{itemize}

Toda \textit{view} gerada via \textit{Interface Builder} é transformada em um arquivo \textit{XML}, representando toda hierarquia e atributos dos componentes. O grande problema é que, diferente de outros plataformas, como o Android, esse \textit{XML} não tem a menor intenção de ser compreendido por seres humanos, o que acaba afetando drasticamente o \textit{code review}, pois não é possível apontar erros ou melhorias. Quando existe conflito de \textit{merge}, isso se agrava tendo que excluir uma das partes e refazê-la depois, sobre o novo código adicionado. A criação de uma \textit{ViewController} é baseada em um \textit{identifier}, uma string, que é definida no \textit{xib} e, caso esse identificador não seja encontrado ou um \textit{IBoutlets} esteja incorreto, a instância falha e o aplicativo é terminado (\textit{crash}), em tempo de execução. Por ser muito suscetível a erro humano e não ser verificada no processo de compilação o \textit{interface builder} acaba se tornando um método inseguro para times e aplicativos grandes.

O \textit{Storyboard} agrava o problema do conflito: por representar um fluxo, existe mais probabilidade de existirem conflitos. Outro grande problema é a injeção de dependência: quando construímos uma \textit{ViewController} baseada no \textit{interface builder}, perdemos a capacidade de usar o seu construtor para passagem de parâmetros, sendo necessário fazer a injeção indireta de dependência por variáveis públicas e opcionais. 

%%% Início de seção. %%%
\subsection{\textit{View Code}}
\label{sec-referencia-views-code}

\textit{View Code} é a implementação de \textit{Views} utilizando a mesma linguagem de programação escolhida para o desenvolvimento do \textit{App}. Através da biblioteca UIKit, fornecida pela Apple, é possível criar, configurar e manipular todos os componentes gráficos que precisamos. O \textit{View Code} é muito mais flexível para customização e animação das \textit{views}, alguns layouts mais complexos não seriam possíveis de serem implementados via \textit{Interface Builder}. Por usar a mesma linguagem de desenvolvimento e ser totalmente procedural, o \textit{View Code} torna possível o processo de \textit{code review} e resolução de conflitos de \textit{merge}, além de solucionar o problema da injeção de dependência no construtor e todo código ser analisado pelo compilador, acusando qualquer erro de tipo em tempo de compilação.

%%% Início de seção. %%%
\section{\textit{Clean Code} e \textit{Clean Architecture}}
\label{sec-ref-clean}

\textit{Clean Code} e \textit{Clean Architecture} são livros escritos por Robert Cecil Martin, que abordam conceitos e técnicas para desenvolvimento de software com qualidade. Mais do que isso, eles também advogam sobre a relação entre processo de desenvolvimento e qualidade final do produto.

O \textit{Clean Code} aborda técnicas que facilitam a leitura e escrita de código, tornando a manutenção e adição de funcionalidades cada vez mais simples. O principal foco é ensinar a reconhecer e escrever boas funções, tendo como fundamento que um sistema é composto por um conjunto de funções e uma estrutura sólida é fundamental para apoiar toda qualidade do sistema.

O \textit{Clean Architecture} de certa forma olha para um nível mais alto, agora não só preocupado com suas funções mas também em como elas são agrupadas e interagem entre si através de uma arquitetura. O autor apresenta os princípios de uma boa arquitetura, e propõe uma conhecida como \textit{Clean Architecture}.

Esses conceitos foram essenciais no desenvolvimento do trabalho e influenciaram várias decisões de abordagens e implementações ao longo do desenvolvimento.

%%% Início de seção. %%%
\section{Scrum}
\label{sec-Scrum}

Scrum é um dos principais \textit{frameworks} utilizado para organizar e gerenciar projetos utilizando os valores e princípios do manifesto ágil. Pode ser combinado com outras ferramentas e métodos como um quadro  \textit{kanban}~\cite{scrum}, para representação dos estados e tarefas. Primeiro iremos apresentar algumas palavras chaves para o entendimento desse modelo e depois iremos introduzir um ciclo básico do seu funcionamento.

\begin{itemize}

\item \textbf{\textit{Product Owner}}: é a pessoa que define os itens que compõem o \textit{Product Backlog} e os prioriza nas \textit{Sprint Planning Meetings}. As tarefas devem ser divididas de forma que, no máximo, ocupem o tempo de uma \textit{Sprint};

\item \textbf{\textit{Scrum Team}}: é a equipe de desenvolvimento;

\item \textbf{\textit{Product Backlog}}: é uma lista contendo todas as funcionalidades desejadas para o produto;

\item \textbf{\textit{Sprint Backlog}}: é uma lista de tarefas que o \textit{Scrum Team} se compromete a fazer em um \textit{Sprint};

\item \textbf{\textit{Sprint Planning Meeting}}: é uma reunião na qual estão presentes o \textit{Product Owner} e todo o \textit{Scrum Team}, nela o \textit{Product Owner} descreve as funcionalidades de maior prioridade para a equipe e em conjunto definem o tempo necessário para cada atividade;

\item \textbf{\textit{Sprints}}: representa uma janela de tempo dentro da qual um conjunto de atividades, elencadas no \textit{Sprint Backlog}, devem ser executadas;

\item \textbf{\textit{Sprint Retrospective}}: ocorre ao final de um \textit{Sprint} e serve para identificar o que funcionou bem, o que pode ser melhorado e que ações serão tomadas para melhorar;

\item \textbf{\textit{Story Point}}: é uma unidade subjetiva de estimativa utilizada por times ágeis para estimar o tempo de uma tarefa, a sua distribuição costuma levar em consideração que quanto mais pontos uma tarefa possui mais chance da estimativa estar errada, por isso é muito comum usar valores da sequência Fibonacci: 1, 2, 3, 5, 8.

\end{itemize}

O \textit{Product Owner} faz a separação e priorização das tarefas atribuindo, \textit{Story Points} que caiba na janela de tempo de uma \textit{Sprint}, depois essas tarefas são cadastradas no \textit{Product Backlog}, esse processo na maior parte das vezes ocorre no início do projeto. 

A cada nova \textit{Sprint} o \textit{Product Owner} separa as tarefas que serão movidas para a \textit{Sprint Backlog}. Em seguida, as tarefas são atribuídas na \textit{Sprint Planning Meeting} para o \textit{Scrum Team}. Ao final da divisão e discussão das tarefas, que podem ter seu \textit{Story Point} alterado por observações do time, acontece a \textit{Sprint Retrospective}, esse processo se repete de acordo com o tempo definido em cada \textit{Sprint}.  Também é muito comum ocorrerem reuniões diárias rápidas para atualizar o time sobre o andamento das tarefas e resolver possíveis dúvidas ou impedimentos, essas cerimônias são chamadas de \textit{Daily Meeting}.

%%% Início de seção. %%%
\section{Ferramentas}
\label{sec-tools}
Após definir os conceitos, tivemos que encontrar ferramentas que nos auxiliassem a administrar e manter nossos \textit{workflows}. Além de atender as demandas necessárias de cada fluxo é ideal que as ferramentas possam ser integráveis entre si e automatizadas. Em uma análise de custo vs. benefício, definimos as seguintes ferramentas para nosso projeto:

\subsection{\textit{Git Project}}
\label{sec-tools-manager}
Para os fluxos gerência e desenvolvimento usamos o \textit{Git Project},\footnote{\url{https://github.com/features/project-management/}} um serviço Web que oferece diversas funcionalidades aplicadas ao git e a gestão de projetos. Para o fluxo de desenvolvimento cada funcionalidade foi definida como uma \textit{milestone} e as tarefas dessa funcionalidade são \textit{issues}. Ao criar uma \textit{issue}, o \textit{Product Owner} escreve uma descrição e atribui \textit{Story Points}, com isso compomos nosso \textit{Product Backlog}.

Para o fluxo de desenvolvimento criamos quadros representando os status da tarefa. Após a \textit{Sprint Planning Meeting}, atribuímos uma \textit{issue} a um desenvolvedor e ela entra no git project, para ser realizada durante a \textit{Sprint}. Conforme a tarefa evolui ela tramita pelos quadros.

\subsection{Git}
\label{sec-tools-git}
Git é um sistema de controle de versão \textit{open source} originalmente desenvolvido em 2005 por Linus Torvalds. Tendo uma arquitetura distribuída, o Git é um exemplo de um DVCS (\textit{Distributed Version Control System}). Em vez de ter apenas um único local para o histórico de versões, como é comum em sistemas como CVS\footnote{\url{https://pt.wikipedia.org/wiki/CVS}} e SVN,\footnote{\url{https://pt.wikiversity.org/wiki/Subversion_-_SVN}} no Git, a cópia de trabalho de cada desenvolvedor também é um repositório que pode conter o histórico completo de todas as alterações. Ele facilita o trabalho colaborativo entre desenvolvedores, para o nosso projeto utilizamos o \textit{Gitflow} (fluxo Git) ilustrado na Figura~\ref{fig-git-flow}.

\begin{figure}[h]
\centering
\includegraphics[width=.80\textwidth]{figuras/referencial/git-flow.png} 
\caption{Git Workflow.}
\label{fig-git-flow}
\end{figure}

Nessa configuração o \textit{branch} (ramo) \textit{master} representar o histórico de publicações do aplicativo, versões lançados na \textit{AppStore} ou no \textit{TestFlight}, o \textit{branch} \textit{develop} é uma ramificação da \textit{master} e serve para integração das funcionalidades. 

Para cada funcionalidade é criado uma \textit{branch} a partir da \textit{develop}, e todas as  suas tarefas tem como origem e fim a sua \textit{branch} de funcionalidade correspondente, quando são desenvolvidas todas as tarefas de uma funcionalidade ela sofre \textit{merge} (fusão) na \textit{develop}. Quando temos um conjunto de funcionalidades que devem ir para produção, fazemos o \textit{merge} da \textit{develop} com a \textit{master}, criando uma nova versão. Tarefas menores que não se encaixam no escopo de funcionalidade são desenvolvidas num \textit{branch} \textit{feature} fixo, seguindo o mesmo fluxo. Só existe um fluxo que foge desse padrão, que são os \textit{hotfix} (correção de falhas que demandam certa urgência): neste caso é criada uma \textit{branch} a partir da \textit{master} e, feita a correção, fazemos o \textit{merge} na \textit{master}.

Do ponto de vista do desenvolvedor ele cria uma \textit{branch} a partir da funcionalidade da sua tarefa e atribui um nome, seguindo o padrão \texttt{[<Abrevição\-Da\-Funcionalidade>-\-<Id\-Da\-Tarefa>]-\-<Nome\-Da\-Tarefa>} e, após desenvolvida, realiza um \textit{Pull Request} para \textit{branch} de origem. Um \textit{Pull Request} é uma maneira de informar à equipe sobre as alterações enviadas para uma \textit{branch}. Pelo Git podemos automatizar algumas tarefas, como fazer a tramitação automática no git project entre algumas etapas, criar um \textit{template} para cada vez em que é aberto um novo \textit{Pull Request},  só permitir o \textit{merge} após o código ser revisado por um número mínimo de pessoas, etc.


\subsection{IDE (\textit{Integrated Development Environment})}
\label{sec-tools-ide}
Como IDE utilizamos o Xcode\footnote{\url{https://developer.apple.com/xcode/}.} que, atualmente, é o mais completo e avançado ambiente de desenvolvimento para produtos Apple. O Xcode foi lançado pela Apple em 2003 e contém um conjunto de ferramentas que auxiliam o desenvolvimento de software, para macOS, iPadOS, watchOS e tvOS. Nele podemos encontrar um conjunto de simuladores que permitem exibir os projetos, nos mais diversos aparelhos da fabricante. 

Variando até mesmo a versão do sistema operacional, é possível testar várias situações e comportamentos, como mudar a localização, conexão à Internet ruim, pouca memória no dispositivo, rotação do dispositivo, etc. Outro destaque são os testes, sendo possível desenvolver e executar testes unitários e de interface. Existem diversos outras funções que nos permitem desenvolver com mais velocidade e praticidade, como \textit{autocomplete}, documentação, visualização da hierarquia de \textit{views}, construção de \textit{layout} via \textit{Storyboard}, entre outras.

\subsection{Teste}
\label{sec-tools-test}
Usamos o próprio Xcode como ferramenta para desenvolver nossos testes. Além de ser bem completo, uma ferramenta nativa evita problemas de suporte e dependências com bibliotecas de terceiros. Uma de nossas bases, para qualidade dos testes, será uma métrica fornecida pela própria IDE que verifica a porcentagem de código coberto por testes.

\subsection{CI e CD}
\label{sec-tools-Cx}
O \textit{Bitrise}\footnote{\url{https://www.bitrise.io}} em conjunto com scripts do \textit{fastlane}\footnote{\url{https://github.com/fastlane/fastlane}} compuseram nossa ferramenta para implantar os serviços de CI e CD. A grande vantagem é a integração com o GitHub. A cada \textit{Pull Request} aberto o \textit{Bitrise} executa os testes automatizados e só aprova se todos forem executados com sucesso, caso contrário o \textit{merge} é bloqueado. Ao fazer o \textit{merge} da \textit{develop} com a \textit{master} e criar uma \textit{tag} o CD pública o \textit{build} na \textit{AppStore}. 
