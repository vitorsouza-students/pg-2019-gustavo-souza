import UI
import UIKit

class NotificationCellHeader: View {
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.spacing = 3
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        return stackView
    }()
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: Font.title, weight: .semibold)
        label.textColor = Palette.darkGrey.color
        
        return label
    }()
    
    private lazy var informationLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: Font.subtitle, weight: .semibold)
        label.textColor = Palette.lightGrey.color
        
        return label
    }()
    
    // ....
    
    override func addComponents() {
        stackView.addArrangedSubview(nameLabel)
        stackView.addArrangedSubview(informationLabel)
        addSubview(stackView)
    }
    
    override func layoutComponents() {
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: Layout.margin),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Layout.margin),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    public func configureView(name: String, information: String) {
        nameLabel.text = name
        informationLabel.text = information
    }
    //...
}
