import XCTest

class SpeakerServiceMock: SpeakerServicing {
    var isEmpty: Bool = false
    
    func getAllSpeakers(completion: @escaping(Result<[Speaker], EventuError>) -> Void) {
        guard isEmpty else {
            //...
            return
        }
        
        completion(.success([]))
    }
    
    //...
}

class SpeakerPresenterSpy: SpeakerPresenting {
    private(set) var callUpdateDataCount: Int = 0
    private(set) var data: [Speaker]?
    
    func updateData(model: [Speaker]) {
        callPerformActionCount += 1
        self.data = model
    }
    
    //...
}

class SpeakerInteractorTest: XCTestCase {
    private let presenterSpy = SpeakerPresenterSpy()
    private let serviceMock = SpeakerServiceMock()
    
    private lazy var sut: SpeakerInteractor = {
        let sut = SpeakerInteractorinit(service: serviceMock, presenter: presenterSpy)
        return sut
    }()
    
    func testUpdateView_WhenDataIsEmpty_ShouldCallUpdateData() {
        serviceMock.isEmpty = true
        
        sut.updateView()
        
        XCTAssertEqual(presenterSpy.callUpdateDataCount, 1)
        XCTAssertTrue(presenterSpy.data.isEmpty)
    }
}
