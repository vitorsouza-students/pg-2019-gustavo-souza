import XCTest

class SpeakerInteractorTest: XCTestCase {
    func testUpdateView_WhenDataIsEmpty_ShouldCallUpdateData() {
        // ...
        sut.updateView(data: [])
        // ...
    }
}
