# Uncomment the line if you want fastlane to automatically update itself
# update_fastlane

default_platform(:ios)

platform :ios do
  desc "Create app on Apple Developer and App Store Connect sites"
  lane :create_app do
    produce
  end

  desc "Match Dev"
  lane :match_dev do
    match(
      type: "appstore",
      app_identifier: [
        "br.com.gsas.Eventu"
      ]
    )
  end

  desc "Run App for beta"
  lane :beta do
    sync_code_signing(type: "appstore")

    gym(
      workspace: "Eventu.xcworkspace",
      scheme: "Eventu",
      include_bitcode: true,
      export_options: {
        method: "app-store",
        provisioningProfiles: {
          "br.com.gsas.Eventu" => "match AppStore br.com.gsas.Eventu",
        }
      }
    )

    pilot(
      skip_waiting_for_build_processing: true,
      beta_app_review_info: {
        contact_email: "gustavo.storck1@gmail.com",
        contact_first_name: "Gustavo",
        contact_last_name: "Storck",
        contact_phone: "27997053363",
        demo_account_name: "teste",
        demo_account_password: "teste12345",
        notes: "Essa é uma versão do Eventu apontando para o ambiente de Homologação, para fins de teste."
      }
    )
  end

  desc "Run Tests"
  lane :tests do
    scan(
      workspace: "Eventu.xcworkspace",
      devices: ["iPhone 11 Pro Max"],
      scheme: "Eventu",
      reinstall_app: true,
      clean: true
    )
  end
end
